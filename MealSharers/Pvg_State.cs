﻿/* Author:  Adam Buchalik 40270304
   Subject: Software Enineering Methods Coursework2
   Title:   Meal Sharers */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MealSharers
{
    class Pvg_State
    {
        private string pvg_status;

        public string Pvg_status
        {
            get { return pvg_status; }
            set { pvg_status = value; }
        }


        public Pvg_State()
        {
        }


        //three methods for every state run from switch
        public void Pvg_state_OK()
        {
            pvg_status = "OK";
        }

        public void Pvg_state_Awaiting()
        {
            pvg_status = "Awaiting Results";
        }

        public void Pvg_state_Rejected()
        {
            pvg_status = "Rejected";
        }

        //method run in AuthenticationWindow with passed string from pvg combo
        public void Pvg_switch()
        {
            switch (AuthenticationWindow.pvg_choice)
            {
                case "OK":
                    Pvg_state_OK();
                    break;
                case "Awaiting Results":
                    Pvg_state_Awaiting();
                    break;
                case "Rejected":
                    Pvg_state_Rejected();
                    break;
            }
        }
    }
}