﻿/* Author:  Adam Buchalik 40270304
   Subject: Software Enineering Methods Coursework2
   Title:   Meal Sharers */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MealSharers
{
    class Hygiene_State
    {
        public string hygiene_status { get; set; }

        public Hygiene_State()
        {
        }

        //three methods for every state run from switch
        public void Hygiene_state_OK()
        {
            hygiene_status = "OK";
        }

        public void Hygiene_state_Renewal()
        {
            hygiene_status = "Renewal in 3 months";
        }

        public void Hygiene_state_None()
        {
            hygiene_status = "None";
        }

        //method run in AuthenticationWindow with passed string from pvg combo
        public void Hygiene_Switch()
        {
            switch (AuthenticationWindow.hygiene_choice)
            {
                case "OK":
                    Hygiene_state_OK();
                    break;
                case "Renewal in 3 months":
                    Hygiene_state_Renewal();
                    break;
                case "None":
                    Hygiene_state_None();
                    break;
            }
        }
    }
}