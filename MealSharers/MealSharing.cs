﻿/* Author:  Adam Buchalik 40270304
   Subject: Software Enineering Methods Coursework2
   Title:   Meal Sharers */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MealSharers
{
    class MealSharing
    {
        private List<Eater> listOfEaters = new List<Eater>();

        public List<Eater> ListOfEaters
        {
            get { return listOfEaters; }
            set { listOfEaters = value; }
        }

        // Eater get/set (no instance here)
        public Eater Eater { get; set; }


        //Adding Eater 
        public void AddEater(string name, int age, string address, string notes, string mailToTrustedPerson)
        {
            Eater = new Eater();
            this.Eater.Name = name;
            this.Eater.Age = age;
            this.Eater.Address = address;
            this.Eater.Notes = notes;
            this.Eater.TrustedContact = mailToTrustedPerson;
            this.AddEaterToTheList(Eater);
        }


        public void AddEaterToTheList(Eater eater)
        {
            listOfEaters.Add(eater);
        }


        public MealSharing()
        {
            AddEater("Andrew Murray", 78, "Edinburgh 23 Lothian Road", "Diabetee", "mydaughter@yahoo.co.uk");
            AddEater("Paul Colins", 72, "Edinburgh 28/3 Lothian Road", "vegan", "myDarling@gmail.com");
            AddEater("Mary Robbins", 69, "Edinburgh 3 Lindsay Road", "Likes sausages", "mySon@gmail@com");
            AddEater("Stanislaw Ogorek", 65, "Edinburgh 78 Easter Road", "big haggis fan", "johnMySon@yahoo.co.uk");
            AddEater("Andrew Murray", 72, "Edinburgh 98 Constitution Street", "hates brocoli", "yasmeenG55@gmail.co.uk");
            AddEater("Stephen Cook", 78, "Edinburgh 54/8 McDonald Road", "Eats everything", "myNiece@hotmail.co.uk");
        }
    }
}