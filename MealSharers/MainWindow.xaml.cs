﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace MealSharers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private AuthenticationWindow authenticationWindow;

        /* Cook donald = new Cook();*/

        MealSharing mealSharing = new MealSharing();


        public string Offer { get; set; }


        int intRate = 0;
        int intCount = 1;
        int Rate = 0;
        string experienceRating = "";


        public MainWindow()
        {
            InitializeComponent();
            authenticationWindow = new AuthenticationWindow();
            authenticationWindow.ShowDialog();

            if (authenticationWindow.label_hygiene.Content.ToString() == "Hygiene: None")
            {
                ComboEaters.Visibility = Visibility.Hidden;
                label_SelectEater.Content = "Your Hygiene Certification is out of date. You can't share a meal!";
            }

            if (authenticationWindow.label_pvg.Content.ToString() == "PVG: Awaiting Results")
            {
                ComboEaters.Visibility = Visibility.Hidden;
                label_SelectEater.Content = "You are still waiting for your PVG certificate. You can't share a meal";
            }


            LoadImages();
            lblRating.Text = intRate.ToString();


            foreach (Eater e in mealSharing.ListOfEaters)
            {
                ComboEaters.Items.Add(e.Name + ", " + e.Age + ",  " + e.Address + ",  " + e.Notes);
            }
        }

        private void button_open_image_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "JPG(*.JPG)|*.jpg";
            f.Title = "Load your photo";

            if (f.ShowDialog() == true)
            {
                Picturebox.Source = new BitmapImage(new Uri(f.FileName));
            }
        }

        private void button_sendOffer_Click(object sender, RoutedEventArgs e)
        {
            if (ComboEaters.SelectedIndex > -1)
            {
                textBox_mealDescription.SelectAll();
                Offer = "EATER: " +
                        ComboEaters.SelectedItem.ToString() + "\n\nMEAL DESCRIPTION: " +
                        textBox_mealDescription.Selection.Text;


                MessageBox.Show(Offer, "MEAL OFFER SENT", MessageBoxButton.OK, MessageBoxImage.Information);

                MessageBox.Show("Log has been produced", "LOG", MessageBoxButton.OK, MessageBoxImage.Information);

                MessageBox.Show("Message to your contact has been sent", "Message to trusted contact",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Sorry, you cannot share a meal!", "SHARING ERROR", MessageBoxButton.OK,
                    MessageBoxImage.Stop);
            }
        }

        //populating grid with start images
        private void LoadImages()
        {
            for (int i = 1; i <= 5; i++)
            {
                Image img = new Image();
                img.Name = "imgRate" + i;
                img.Stretch = Stretch.UniformToFill;
                img.Height = 45;
                img.Width = 45;
                img.Source = new BitmapImage(new Uri(@"\MinusRate.png", UriKind.Relative));
                img.MouseEnter += imgRateMinus_MouseEnter;
                pnlMinus.Children.Add(img);

                Image img1 = new Image();
                img1.Name = "imgRate" + i + i;
                img1.Stretch = Stretch.UniformToFill;
                img1.Height = 45;
                img1.Width = 45;
                img1.Visibility = Visibility.Hidden;
                img1.Source = new BitmapImage(new Uri(@"\PlusRate.png", UriKind.Relative));
                img1.MouseEnter += imgRatePlus_MouseEnter;
                img1.MouseLeave += imgRatePlus_MouseLeave;
                img1.MouseLeftButtonUp += imgRatePlus_MouseLeftButtonUp;
                pnlPlus.Children.Add(img1);
            }
        }

        private void imgRateMinus_MouseEnter(object sender, MouseEventArgs e)
        {
            GetData(sender, Visibility.Visible, Visibility.Hidden);
        }

        private void imgRatePlus_MouseEnter(object sender, MouseEventArgs e)
        {
            GetData(sender, Visibility.Visible, Visibility.Hidden);
        }

        private void imgRatePlus_MouseLeave(object sender, MouseEventArgs e)
        {
            GetData(sender, Visibility.Hidden, Visibility.Visible);
        }

        private void gdRating_MouseLeave(object sender, MouseEventArgs e)
        {
            SetImage(Rate, Visibility.Visible, Visibility.Hidden);
        }

        private void GetData(object sender, Visibility imgYellowVisibility, Visibility imgGrayVisibility)
        {
            GetRating(sender as Image);
            SetImage(intRate, imgYellowVisibility, imgGrayVisibility);
        }

        private void SetImage(int intRate, Visibility imgYellowVisibility, Visibility imgGrayVisibility)
        {
            foreach (Image imgItem in pnlPlus.Children.OfType<Image>())
            {
                if (intCount <= intRate)
                    imgItem.Visibility = imgYellowVisibility;
                else
                    imgItem.Visibility = imgGrayVisibility;
                intCount++;
            }
            intCount = 1;
        }

        private void imgRatePlus_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            GetRating(sender as Image);
            Rate = intRate;
            lblRating.Text = intRate.ToString();
        }

        private void GetRating(Image Img)
        {
            string strImgName = Img.Name;
            intRate = Convert.ToInt32(strImgName.Substring(strImgName.Length - 1, 1));
        }

        //method changing stars to text representation of rating
        private void button_shareExperience_Click(object sender, RoutedEventArgs e)
        {
            switch (Rate)
            {
                case 0:
                    experienceRating = "A Disaster!";
                    break;
                case 1:
                    experienceRating = "Well, nothing special! \u2605";
                    break;
                case 2:
                    experienceRating = "Not bad, could be better though \u2605\u2605";
                    break;
                case 3:
                    experienceRating = "That was OK \u2605\u2605\u2605";
                    break;
                case 4:
                    experienceRating = "Very good meal companion \u2605\u2605\u2605\u2605 ";
                    break;
                case 5:
                    experienceRating = "I strongly recommend, Marvelous Experience \u2605\u2605\u2605\u2605\u2605 ";
                    break;
            }


            if (ComboEaters.SelectedIndex == -1)
            {
                MessageBox.Show("Please select the Eater from a list", "EATER NOT SELECTED!", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
            }
            else
            {
                MessageBox.Show(
                    "RATED EATER: " + ComboEaters.SelectedItem.ToString() + "\n\nMY RATE: " + experienceRating,
                    "RATING HAS BEEN SENT", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void ComboEaters_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
    }
}