﻿/* Author:  Adam Buchalik 40270304
   Subject: Software Enineering Methods Coursework2
   Title:   Meal Sharers */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MealSharers
    {
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class AuthenticationWindow : Window
        {

       

        public static string pvg_choice;
        public static string hygiene_choice;
        //string needed to send a log
        public string cookDetails;

        Pvg_State pvg = new Pvg_State();
        Hygiene_State hygiene = new Hygiene_State();

        Cook donald = new Cook();

        // if false window isn't closing. In signIn handler changed to true if password OK
        public bool closingStatus { get; set; }
        
        public AuthenticationWindow()
            {
            InitializeComponent();

            donald.Name = "Donald Trump";
            donald.Age = 71;
            donald.Address = "725 5th Ave, New York, NY 10022, USA";
            donald.Pass = "pass";
            donald.Username = "cook";
            donald.Cook_Pvg_status = pvg.Pvg_status;
            donald.Cook_Hygiene_Status = hygiene.hygiene_status;

                cookDetails = "Cook Name: " + donald.Name +
                              "\nCook Address: " + donald.Address +
                              "\nCriminal Record Check: " + donald.Cook_Pvg_status +
                              "\nHygiene Certificate Status: " + donald.Cook_Hygiene_Status;

            //prevent login window from closing
            closingStatus = false;
            }
        //blocking window from closing
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if ( pvg_choice == null || hygiene_choice == null || pvg_choice == "Rejected" )
            {
                e.Cancel = true;
                MessageBox.Show("Sorry, you cannot close a login window. Please, Log In, Sign Up or Close Application", "WINDOW CLOSING", MessageBoxButton.OK, MessageBoxImage.Warning);

            }
            else if (closingStatus == true)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
           
           
        }

        private void button_signIn_Click(object sender, RoutedEventArgs e)
        {
           
             
            if (pvg_choice == null || hygiene_choice == null)
            {
            MessageBox.Show("This is prototype version. Normally your certificates checking happens in background. Now you have to select certificates manually with button in top right corner called 'Get Certificates'", "CERTIFICATION", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                if (label_username.Text == donald.Username && passwordBox_password.Password.ToString() == donald.Pass)
                {
                    closingStatus = true;
                    this.Close();
                }

                else
                {
                    MessageBox.Show("Sorry, Username and Password don't match or some of them is empty", "PASSWORD ERROR", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }

        }

        private void button_signUp_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("This is an app prototype!\nThe only option available is 'Sign In' ", "SIGN UP", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void button_closeProgram_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }





       
        //adding list of item to pvg combo
        private void Combo_PVG_Loaded(object sender, RoutedEventArgs e)
            {
            List<string> pvg_data = new List<string>();
            pvg_data.Add("OK");
            pvg_data.Add("Awaiting Results");
            pvg_data.Add("Rejected");
            var pvg_combo = sender as ComboBox;
            pvg_combo.ItemsSource = pvg_data;
            pvg_combo.SelectedIndex = -1;
            }

        private void Combo_PVG_SelectionChanged(object sender, SelectionChangedEventArgs e)
            {
            pvg_choice = Combo_PVG.SelectedItem.ToString();
            pvg.Pvg_switch();
            label_pvg.Content = "PVG: "+ pvg.Pvg_status;
            Pvg_Colores();
            }


        public void Pvg_Colores()
            {
            switch (pvg_choice)
                {
                case "OK": label_pvg.Background = Brushes.LightGreen;
                    label_pvg.Foreground = Brushes.Black; break;
                case "Awaiting Results": label_pvg.Background = Brushes.Orange;
                    label_pvg.Foreground = Brushes.Black; break;
                case "Rejected": label_pvg.Background = Brushes.Red; label_pvg.Foreground = Brushes.White; break;
                }
            }

        private void Combo_Hygiene_SelectionChanged(object sender, SelectionChangedEventArgs e)
            {
            hygiene_choice = Combo_Hygiene.SelectedItem.ToString();
            hygiene.Hygiene_Switch();
            label_hygiene.Content = "Hygiene: " + hygiene.hygiene_status;
            Hygiene_Colores();
            }

        private void Combo_Hygiene_Loaded(object sender, RoutedEventArgs e)
            {
            List<string> hygiene_data = new List<string>();
            hygiene_data.Add("OK");
            hygiene_data.Add("Renewal in 3 months");
            hygiene_data.Add("None");
            var hygiene_combo = sender as ComboBox;
            hygiene_combo.ItemsSource = hygiene_data;
            hygiene_combo.SelectedIndex = -1;
            }


        public void Hygiene_Colores()
            {
            switch (hygiene_choice)
                {
                case "OK": label_hygiene.Background = Brushes.LightGreen;
                    label_hygiene.Foreground = Brushes.Black; break;
                case "Renewal in 3 months": label_hygiene.Background = Brushes.Orange;
                    label_hygiene.Foreground = Brushes.Black; break;
                case "None": label_hygiene.Background = Brushes.Red;
                    label_hygiene.Foreground = Brushes.White; break;
                }
            }



        //making certification canvas visible
        private void ButtonCertificate_Click(object sender, RoutedEventArgs e)
        {
            if (Canvas_Certificate.Visibility == Visibility.Visible)
            {
                Canvas_Certificate.Visibility = Visibility.Hidden;
            }
            else
            {
            Canvas_Certificate.Visibility = Visibility.Visible;
            }
        }

        private void button_CloseApp_Click(object sender, RoutedEventArgs e)
        {
            closingStatus = true;
            Application.Current.Shutdown();
            }




        }
    }
